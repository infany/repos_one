CREATE PROCEDURE checkReader
(
	@idReader int
)
AS
BEGIN
	DECLARE @t bit;
	SELECT @t = Active FROM Reader WHERE idReader = @idReader;
	IF @t = 0 OR @t IS NULL 
		BEGIN
			THROW 51000, 'Reader deactivated', 1;
			RETURN;
		END
END	