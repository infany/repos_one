CREATE PROCEDURE addBookLease
(
	@idBook int
	,@idReader int
	,@DateOut date
)
AS
BEGIN
	EXEC checkBook @idBook;
	EXEC checkReader @idReader;
	IF @DateOut < (SELECT ReleaseDate FROM Book WHERE idBook = @idBook)
		BEGIN
			THROW 51000, 'ReleaseDate > DateOfTran', 1;
			RETURN
		END
	INSERT INTO BookLease(idBook, idReader, DateOut)
		VALUES(@idBook, @idReader, @DateOut)
END