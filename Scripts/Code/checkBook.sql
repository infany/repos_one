CREATE PROCEDURE checkBook
(
	@idBook int
)
AS
BEGIN
	DECLARE @t bit;
	SELECT @t = Active FROM Book WHERE idBook = @idBook;
	IF @t = 0 OR @t IS NULL 
		BEGIN
			THROW 51000, 'Book deactivated', 1;
			RETURN;
		END
END	