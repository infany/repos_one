CREATE PROCEDURE lossBook
(
	@idBookLease int
	,@DateIn date
)
AS
BEGIN
	IF (SELECT DateIn FROM BookLease WHERE idBookLease = @idBookLease) IS NOT NULL
		BEGIN 
			THROW 51000, 'DateIn already exists', 1;
			RETURN;
		END
	DECLARE @temp date;
	SELECT @temp = DateOut FROM BookLease WHERE idBookLease = @idBookLease
	IF @temp > @DateIn
		BEGIN
			THROW 51000, 'DateIn less then DateOut', 1;
			RETURN;
		END
		
	UPDATE BookLease SET DateIn = @DateIn WHERE idBookLease = @idBookLease;
	DECLARE @idBook1 int;
	SELECT @idBook1 = idBook FROM BookLease WHERE idBookLease = @idBookLease;
	EXEC addBookMovement @idBook1, @DateIn, 'OUTCOME', 1;
	
END