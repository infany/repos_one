CREATE PROCEDURE updateTagBook
(
	@idBook int
	,@TagList nvarchar(512)
)
AS
BEGIN
	EXEC checkBook @idBook;
	DELETE FROM [Tag-Book] WHERE idBook = @idBook;
	
	INSERT INTO Tag(TagName) 
	SELECT TagName = LOWER(LTRIM(RTRIM(value))) from STRING_SPLIT(@TagList, ',')
	WHERE not exists (select a.TagName from Tag a where a.TagName = value)
	
	
	INSERT INTO [Tag-Book](idBook, idTag)
	SELECT a.idBook, b.idTag from 
	(SELECT idBook FROM Book 
		WHERE idBook = @idBook) a,
	(SELECT idTag FROM Tag 
		WHERE TagName IN (SELECT LOWER(LTRIM(RTRIM(value))) FROM STRING_SPLIT(@TagList, ','))) b
END