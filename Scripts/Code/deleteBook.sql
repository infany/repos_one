CREATE PROCEDURE deleteBook
(
	@idBook int
)
AS
BEGIN
	EXEC checkBook @idBook;
	UPDATE Book SET Active = 0 WHERE idBook = @idBook;
END