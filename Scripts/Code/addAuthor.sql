CREATE PROCEDURE addAuthor
(
	@FullName nvarchar(128)
	,@DateOfBirth date
	,@Country nvarchar(128)
)
AS
BEGIN
	INSERT INTO Author(FullName, DateOfBirth, Country)
		VALUES (@FullName, @DateOfBirth, @Country)
END