CREATE PROCEDURE addBookMovement
(
	@idBook int
	,@DateOfTran date
	,@TypeOfTran nvarchar(128)
	,@NumberOfBook int
)
AS
BEGIN
	EXEC checkBook @idBook;
	
	IF @DateOfTran < (SELECT ReleaseDate FROM Book WHERE idBook = @idBook)
		BEGIN
			THROW 51000, 'ReleaseDate > DateOfTran', 1;
			RETURN
		END
	
	IF @NumberOfBook < 0
		BEGIN
			THROW 51000, 'Number of book < 0', 1;
			RETURN
		END
	
	IF @TypeOfTran != 'INCOME' AND @TypeOfTran != 'OUTCOME'
		BEGIN
			THROW 51000, 'Unknown TRANSACTION(INCOME or OUTCOME)', 1;
			RETURN
		END
		
	IF @TypeOfTran = 'OUTCOME' AND @NumberOfBook != 1
		BEGIN
			THROW 51000, 'IF OUTCOME THEN NumberOfBook = 1', 1;
			RETURN
		END
		
	INSERT INTO BookMovement(idBook, DateOfTran, TypeOfTran, NumberOfBook)
		VALUES(@idBook, @DateOfTran, @TypeOfTran, @NumberOfBook)
	
END