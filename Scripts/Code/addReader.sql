CREATE PROCEDURE addReader
(
	@FullName nvarchar(128)
	,@PhoneNumber nvarchar(10)
	,@LibraryCard int
)
AS
BEGIN
	
	INSERT INTO Reader(FullName, PhoneNumber, LibraryCard)
		VALUES(@FullName, @PhoneNumber, @LibraryCard)
	
END