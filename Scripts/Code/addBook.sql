CREATE PROCEDURE addBook
(
	@BookName nvarchar(128)
	,@ReleaseDate date
	,@PublishingName nvarchar(128)
	,@idAuthor int
	,@NumberOfPages int
	,@TagList nvarchar(512)
)
AS
BEGIN
	
	DECLARE @idPublishing int;
	SELECT @idPublishing = -1;
	SELECT @idPublishing = idPublishing FROM Publishing WHERE @PublishingName = PublishingName;
	--Если издательства не существует, то добавить его, и записать его ID
	IF @idPublishing = -1
		BEGIN
			INSERT INTO Publishing(PublishingName) VALUES(@PublishingName);
			SELECT @idPublishing = idPublishing FROM Publishing WHERE @PublishingName = PublishingName;
		END;

	--Добавление тегов которые ещё не было
	INSERT INTO Tag(TagName) 
	SELECT TagName = LOWER(LTRIM(RTRIM(value))) from STRING_SPLIT(@TagList, ',')
	WHERE not exists (select a.TagName from Tag a where a.TagName = value)
	
	

	INSERT INTO Book(BookName, ReleaseDate, idPublishing, idAuthor, NumberOfPages)
		VALUES(@BookName, @ReleaseDate, @idPublishing, @idAuthor, @NumberOfPages);
		
	INSERT INTO [Tag-Book](idBook, idTag)
	SELECT a.idBook, b.idTag from 
	(SELECT idBook FROM Book 
		WHERE BookName = @BookName 
		AND ReleaseDate = @ReleaseDate 
		AND idPublishing = @idPublishing
		AND idAuthor = @idAuthor
		AND NumberOfPages = @NumberOfPages) a,
	(SELECT idTag FROM Tag 
		WHERE TagName IN (SELECT LOWER(LTRIM(RTRIM(value))) FROM STRING_SPLIT(@TagList, ','))) b
	
END