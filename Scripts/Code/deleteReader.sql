CREATE PROCEDURE deleteReader
(
	@idReader int
)
AS
BEGIN
	EXEC checkReader @idReader;
	UPDATE Reader SET Active = 0 WHERE idReader = @idReader;
END