CREATE PROCEDURE updateBookLease
(
	@idBookLease int
	,@DateIn date
)
AS
BEGIN
	--Не меняем дату если она уже стоит
	IF (SELECT DateIn FROM BookLease WHERE idBookLease = @idBookLease) IS NOT NULL
		BEGIN 
			THROW 51000, 'DateIn already exists', 1;
			RETURN;
		END
	
	--Проверим дату на то что книгу вернули после взятия, а не до
	DECLARE @temp date;
	SELECT @temp = DateOut FROM BookLease WHERE idBookLease = @idBookLease
	IF @temp > @DateIn
		BEGIN
			THROW 51000, 'DateIn less then DateOut', 1;
			RETURN;
		END
		
	--Когда все хорошо, можно добавлять
	UPDATE BookLease SET DateIn = @DateIn WHERE idBookLease = @idBookLease
	--Проверяем нужно ли начислять/снимать бонусы
	DECLARE @diff int;
	SELECT @diff = DATEDIFF(day, DateOut, DateIn) FROM BookLease WHERE idBookLease = @idBookLease
	IF @diff <= 14
		BEGIN
			UPDATE BonusCard SET NumberOfBonuses += 5 
			WHERE idReader = (SELECT idReader FROM BookLease WHERE idBookLease = @idBookLease)
		END
	IF @diff > 30
		BEGIN
			UPDATE BonusCard SET NumberOfBonuses -= 2 
			WHERE idReader = (SELECT idReader FROM BookLease WHERE idBookLease = @idBookLease)
		END
	
	
END