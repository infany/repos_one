--X
--UDF for notReturnedBook
CREATE FUNCTION getCountBook
(
	@idReader int
)
RETURNS INT AS
BEGIN
	DECLARE @temp int;
	SELECT @temp = Count(idBook) from BookLease where idReader = @idReader AND DateIn IS NULL
	RETURN @temp
END
GO
CREATE PROCEDURE notReturnedBook
AS
BEGIN
	SELECT DISTINCT b.FullName AS [Список не вернувших] FROM BookLease a JOIN Reader b ON a.idReader = b.idReader
	WHERE dbo.getCountBook(a.idReader) >= 2
END

--//--