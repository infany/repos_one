CREATE PROCEDURE showAuthors
(
	@Country nvarchar(128)
)
AS
BEGIN
	SELECT idAuthor AS ID, FullName AS [ФИО], DateOfBirth as [Дата рождения]
	FROM Author
	WHERE Country = @Country
	ORDER BY FullName
END