--IX

CREATE PROCEDURE topReader
AS
BEGIN
	--Топ 3 читающих
	SELECT TOP 3 a.idReader, a.FullName AS [Топ самых читающих], COUNT(b.idBookLease) AS [Кол-во взятых книг] FROM Reader a LEFT JOIN BookLease b ON a.idReader = b.idReader
	GROUP BY a.idReader, a.FullName
	ORDER BY [Кол-во взятых книг] DESC
	--Топ 3 нечитающих
	SELECT TOP 3 a.idReader, a.FullName AS [Топ самых нечитающих], COUNT(b.idBookLease) AS [Кол-во взятых книг] FROM Reader a LEFT JOIN BookLease b ON a.idReader = b.idReader
	GROUP BY a.idReader, a.FullName
	ORDER BY [Кол-во взятых книг] 
END

--//---