--VIII

CREATE PROCEDURE topLossBook
AS
BEGIN
	SELECT TOP 5 a.idBook, COUNT(b.idBook) AS [Книг утеряно] FROM BookLease a JOIN BookMovement b ON a.idBook = b.idBook
	WHERE a.DateIn = b.DateOfTran AND b.TypeOfTran = 'OUTCOME'
	GROUP BY a.idBook
	ORDER BY [Книг утеряно] DESC
END

--//---