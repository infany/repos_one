--VI

CREATE PROCEDURE topTags
AS
BEGIN
	SELECT TOP 5 TagName AS [Тег], SUM(c.coun) AS [Сколько раз брали]
	FROM Tag a 
	JOIN [Tag-Book] b ON a.idTag = b.idTag, 
	(SELECT a.idBook, COUNT(DateOut) AS coun 
		FROM Book a 
		JOIN BookLease b ON a.idBook = b.idBook
	GROUP BY a.idBook) c
	WHERE b.idBook = c.idBook
	GROUP BY TagName
	ORDER BY [Сколько раз брали] DESC, TagName
END

--//---