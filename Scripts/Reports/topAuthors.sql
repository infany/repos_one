--VII

CREATE PROCEDURE topAuthors
AS
BEGIN
	SELECT TOP 5 a.FullName AS [ФИО], COUNT(b.idBook) AS [Написано книг] FROM Author a JOIN Book b ON a.idAuthor = b.idAuthor
	GROUP BY a.FullName
	ORDER BY [Написано книг] DESC
END

--//---