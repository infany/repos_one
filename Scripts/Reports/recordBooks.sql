CREATE PROCEDURE recordBooks
AS
BEGIN
	SELECT a.idBook, a.BookName AS [Название], SUM(b.op1) AS [Пришло], SUM(b.op2) AS [Ушло], c.op3 AS [Выдано], c.op4 AS [Возвращено], d.op5 AS [Утеряно], e.sumaa AS [На руках]
	FROM Book a, 
	(SELECT a.idBook, 
	CASE WHEN TypeOfTran = 'INCOME' THEN SUM(NumberOfBook) ELSE 0 END AS op1, 
	CASE WHEN TypeOfTran = 'OUTCOME' THEN SUM(NumberOfBook) ELSE 0 END AS op2
	FROM Book a LEFT JOIN BookMovement b ON a.idBook = b.idBook
	GROUP BY a.idBook, TypeOfTran) b,
	(SELECT a.idBook, COUNT(DateOut) AS op3, COUNT(DateIn) AS op4 FROM Book a LEFT JOIN BookLease b ON a.idBook = b.idBook
	GROUP BY a.idBook) c,
	(SELECT idBook, SUM(counter1) AS op5
	FROM (SELECT a.idBook, a.BookName, CASE WHEN b.DateOfTran = c.DateIn AND b.TypeOfTran = 'OUTCOME' AND b.idBook = c.idBook THEN 1 ELSE 0 END AS counter1
	FROM Book a left JOIN BookMovement b ON a.idBook = b.idBook
	LEFT JOIN BookLease c ON a.idBook = c.idBook) a
	GROUP BY idBook) d,
	(SELECT a.idBook, SUM(a.coun) AS sumaa
	FROM (SELECT a.idBook, CASE WHEN b.DateIn IS NULL THEN COUNT(b.DateOut) ELSE 0 END AS coun
	FROM Book a LEFT JOIN BookLease b ON a.idBook = b.idBook
	GROUP BY a.idBook, DateOut, DateIn) a
	GROUP BY a.idBook) e
	WHERE a.idBook = b.idBook AND a.idBook = c.idBook AND a.idBook = d.idBook AND a.idBook = e.idBook
	GROUP BY a.idBook, BookName, c.op3, c.op4, d.op5, e.sumaa
END