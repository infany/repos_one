--XI

CREATE FUNCTION funcLossMoreThen(@num int)
RETURNS TABLE AS
	RETURN SELECT a.idReader FROM BookLease a JOIN BookMovement b ON a.idBook = b.idBook
	WHERE a.DateIn = b.DateOfTran AND b.TypeOfTran = 'OUTCOME' 
	GROUP BY a.idReader
	HAVING COUNT(b.idBook) > @num
GO
CREATE PROCEDURE lossMoreThen(@num int)
AS
BEGIN
	SELECT idReader as [Список потерявших] FROM dbo.funcLossMoreThen(@num)
END

--//---