--V

CREATE PROCEDURE showReaderWithTags
AS
BEGIN
	--Теги которые читатель не брал
	CREATE TABLE #Table1
	(
		idReader int
		,idTag int
		,countOut int
	)
	
	INSERT INTO #Table1(idReader,idTag)
	SELECT idReader, idTag FROM Reader, Tag
	EXCEPT (SELECT c.idReader, e.idTag
	FROM BookLease c JOIN [Tag-Book] e ON c.idBook = e.idBook
	GROUP BY c.idReader, e.idTag) 
	
	--Теги и их популярность
	CREATE TABLE #Table2
	(
		idTag int
		,countOut int
	)
	INSERT INTO #Table2(idTag, countOut)
	SELECT a.idTag, CASE WHEN SUM(c.coun) > 0 THEN SUM(c.coun) ELSE 0 END AS countOut
	FROM Tag a 
	LEFT JOIN [Tag-Book] b ON a.idTag = b.idTag 
	LEFT JOIN (SELECT a.idBook, CASE WHEN Count(DateOut) > 0 THEN Count(DateOut) ELSE 0 END  AS coun 
			FROM Book a 
			LEFT JOIN BookLease b ON a.idBook = b.idBook GROUP BY a.idBook) c ON b.idBook = c.idBook 
	GROUP BY a.idTag, TagName
	
	UPDATE a SET a.countOut = b.countOut FROM #Table1 a JOIN #Table2 b ON a.idTag = b.idTag
	
	DECLARE @cR int = 0, @cT int = 0, @maxR int;
	
	SELECT @maxR = MAX(idReader), @cR = MIN(idReader) FROM #Table1
	WHILE @cR <= @maxR
	BEGIN
		IF EXISTS(SELECT idReader FROM #Table1 WHERE idReader = @cR)
		BEGIN
			DECLARE @temp_cTag int;
			SELECT  @temp_cTag = COUNT(idTag)  FROM #Table1 WHERE idReader = 1 
			WHILE @temp_cTag > 3
			BEGIN
				DECLARE @t1 int
				SELECT top 1 @t1 = idTag FROM #Table1 WHERE idReader = 1 ORDER BY countOut ;
				DELETE FROM #Table1 WHERE idReader = 1 AND idTag = @t1;
				SELECT  @temp_cTag = COUNT(idTag)  FROM #Table1 WHERE idReader = 1 
			END;
		END;
		SET @cR = @cR + 1;
	END;
		
	SELECT a.idReader, b.FullName [ФИО], STRING_AGG(c.TagName, ',') AS [Список тегов которые не читал]
	FROM #Table1 a 
	JOIN Reader b ON a.idReader = b.idReader 
	JOIN Tag c ON a.idTag = c.idTag
	GROUP BY a.idReader, b.FullName
	DROP TABLE #Table1, #Table2
END


--//---