CREATE PROCEDURE topXBooks
(
	@TagList nvarchar(512)
)
AS
BEGIN
	SELECT TOP 10 a.BookName AS [Название книги], COUNT(DateOut) AS [Количество взятий], a.csv AS [Список тегов]
	FROM (SELECT a.idBook, c.BookName, e.csv FROM [Tag-Book] a, 
		(SELECT idTag FROM string_split(@TagList, ',') a JOIN Tag b ON LOWER(LTRIM(RTRIM(a.value))) = b.TagName) b, Book c, 
		(SELECT a.idBook, STRING_AGG(b.TagName, ',') AS csv FROM Book a, Tag b, [Tag-Book] c WHERE c.idBook = a.idBook AND b.idTag = c.idTag GROUP BY a.idBook) e
		WHERE a.idTag = b.idTag AND a.idBook = c.idBook AND e.idBook = a.idBook) a 
	JOIN BookLease b 
	ON a.idBook = b.idBook
	GROUP BY a.BookName, a.csv
	ORDER BY [Количество взятий] DESC, a.BookName 
END