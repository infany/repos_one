CREATE PROCEDURE showBooks
AS 
BEGIN 
	SELECT a.BookName AS [Название], b.FullName AS [ФИО автора], b.DateOfBirth AS [Дата рождения]
	FROM Book a
	JOIN Author b
	ON a.idAuthor = b.idAuthor
	ORDER BY a.BookName
END