ALTER TABLE Book
ADD CONSTRAINT fk_Book_Publishing
FOREIGN KEY (idPublishing) REFERENCES Publishing(idPublishing)

GO

ALTER TABLE Book
ADD CONSTRAINT fk_Book_Author
FOREIGN KEY (idAuthor) REFERENCES Author(idAuthor)