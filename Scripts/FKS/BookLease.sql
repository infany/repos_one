ALTER TABLE BookLease
ADD CONSTRAINT fk_BookLease_Book
FOREIGN KEY (idBook) REFERENCES Book(idBook)

GO

ALTER TABLE BookLease
ADD CONSTRAINT fk_BookLease_Reader
FOREIGN KEY (idReader) REFERENCES Reader(idReader)
