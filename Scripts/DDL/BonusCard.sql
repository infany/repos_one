CREATE TABLE BonusCard
(
	idBonusCard int NOT NULL IDENTITY(1, 1)
	,idReader int NOT NULL UNIQUE
	,NumberOfBonuses int NOT NULL DEFAULT 0
	,PRIMARY KEY(idBonusCard)
);