CREATE TABLE Author
(
	idAuthor int NOT NULL IDENTITY(1,1)
	,FullName nvarchar(128) NOT NULL
	,DateOfBirth date NULL
	,Country nvarchar(128) NULL
	PRIMARY KEY(idAuthor)
);
