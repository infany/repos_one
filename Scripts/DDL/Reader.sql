CREATE TABLE Reader
(
	idReader int NOT NULL IDENTITY(1,1)
	,FullName nvarchar(128) NOT NULL
	,PhoneNumber nvarchar(10) NULL
	,LibraryCard int NOT NULL UNIQUE
	,Active bit NOT NULL DEFAULT 1
	,PRIMARY KEY(idReader)
);

