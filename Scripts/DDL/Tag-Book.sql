CREATE TABLE [Tag-Book]
(
	[idTag-Book] int NOT NULL IDENTITY(1,1)
	,idBook int NOT NULL
	,idTag int NOT NULL
	PRIMARY KEY([idTag-Book])
);
