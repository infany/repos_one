CREATE TABLE BookMovement
(
	idBookMovement int NOT NULL IDENTITY(1,1)
	,idBook int NOT NULL
	,DateOfTran date NOT NULL
	,TypeOfTran nvarchar(128) NOT NULL
	,NumberOfBook int NOT NULL
	,PRIMARY KEY(idBookMovement)
);

