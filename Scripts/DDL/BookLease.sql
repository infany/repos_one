CREATE TABLE BookLease
(
	idBookLease int NOT NULL IDENTITY(1,1)
	,idBook int NOT NULL
	,idReader int NOT NULL
	,DateOut date NOT NULL
	,DateIn date NULL
	,PRIMARY KEY(idBookLease)
);
