CREATE TABLE Publishing
(
	idPublishing int NOT NULL IDENTITY(1,1)
	,PublishingName nvarchar(128) NOT NULL UNIQUE
	,PRIMARY KEY(idPublishing)
);
