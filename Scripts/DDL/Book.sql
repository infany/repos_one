CREATE TABLE Book
(
	idBook int NOT NULL IDENTITY(1,1)
	,BookName nvarchar(128) NOT NULL
	,ReleaseDate date NULL
	,idPublishing int NOT NULL
	,idAuthor int NOT NULL
	,NumberOfPages int NOT NULL
	,Active bit NOT NULL DEFAULT 1
	PRIMARY KEY(idBook)
);
