CREATE TABLE Tag 
(
	idTag int NOT NULL IDENTITY(1,1)
	,TagName nvarchar(128) NOT NULL UNIQUE
	,TagDescription nvarchar(256) NULL
	PRIMARY KEY(idTag)
);
